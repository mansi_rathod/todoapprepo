import AWS from "aws-sdk";
import { ConfigurationServicePlaceholders } from "aws-sdk/lib/config_service_placeholders";
import { ListTablesOutput } from 'aws-sdk/clients/dynamodb';


AWS.config.update({
  region: "ap-south-1",
});
  var docClient = new AWS.DynamoDB.DocumentClient();
  var dynamodb = new AWS.DynamoDB();
var table = "to-do";
 export const initDatabase = async () => {
  // create a user table - username, id
  // check if table already exists

  try {
    const tables = await dynamodb.listTables() as ListTablesOutput;
    const tableNames = tables.TableNames ? tables.TableNames: [];


    //console.log(tables);
    for (let i = 0; i < tableNames.length; i++) {
      if (tableNames[i] == "to-do") {
        console.log("createTable Done");
        return;
      }
    }
  } catch (error) {
    console.log(error);
    console.log("creating table failed.");
    throw "Check AWS DynamoDB configuration and then run server again.";
  }
  // if no table is there, create a table
  var params = {
    TableName: "to-do",
    KeySchema: [
      { AttributeName: "username", KeyType: "HASH" },
      { AttributeName: "task", KeyType: "RANGE" },

    ],
    AttributeDefinitions: [
      { AttributeName: "username", AttributeType: "S" },
      { AttributeName: "task", AttributeType: "S" },
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5,
    },
  };

  try {
    await dynamodb.createTable(params);
  } catch (error) {
    console.log("creating table failed.");
    throw "Check AWS DynamoDB configuration and then run server again.";
  }
};
initDatabase()
console.log("reached")
