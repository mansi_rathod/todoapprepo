"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var aws_sdk_1 = require("aws-sdk");
var express_1 = require("express");
var userfinal_1 = require("./userfinal");
var app = express_1["default"]();
var port = 3000;
var ts = Date.now();
var docClient = new aws_sdk_1["default"].DynamoDB.DocumentClient();
//dummy
app.get('/', function (req, res) {
    console.log(req.query);
    res.send('Get started');
});
userfinal_1.initDatabase();
//api to create and submit username
app.get('/userfinal', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log(req.query);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, docClient.put({
                        TableName: "to-do",
                        Item: {
                            user_id: ts.toString(),
                            username: req.query.username
                        }
                    })];
            case 2:
                _a.sent();
                res.send({
                    user_id: ts,
                    message: "creating user<" + req.query.username + "> is success",
                    task: "your task : " + req.query.task
                });
                return [3 /*break*/, 4];
            case 3:
                error_1 = _a.sent();
                console.log(error_1);
                res.send({
                    error: "creating username failed"
                });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
/* //api for fetching complete and incomplete todo
var params = {
   TableName: "to-do",
   Key:{
       "username": req.query,
       "status": req.query,
   }
 };
 app.get('/fetchtodo', async (req : Request,res : Response) => {
 
   try{
     await docClient.get(params);
     var passvariable = req.query.username.toString();
     res.send(fetchTodo(passvariable));
   }
   catch (error) {
     console.log(error);
     res.send({
       error: "creating username failed",
     });
   }
 });

 //api for adding todo


var params2 =
{
 TableName : "to-do",
 Item:{
   "username" : req.query,
   "task": req.query,
 }
};
app.get('/addtodo', async(req, res )=> {
 console.log("Username :" ,req.query.username);
 console.log("task :",req.query.task);
try{
   await docClient.put({TableName : "to-do",
   Item:{
     username : req.query.username,
     task: req.query.task,
   } });
 res.send({
   user_id: ts,
   username: req.query.username,
   task : req.query.task,
   message: "Added task is : "+ req.query.username ,
 });
}
  catch (error) {
 console.log(error);
 res.send({
   error: "Adding Todo Failed",
 });
}
});*/
app.listen(port, function () {
    console.log("Example app listening at http://localhost:" + port);
});
