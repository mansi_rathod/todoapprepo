
import AWS from 'aws-sdk';
import bodyParser from "body-parser";
import {fetchTodo} from "./fetchtodo"
import express, { request, Request, Response } from 'express' 
import {initDatabase} from "./userfinal"

const app = express();
const port = 3000;
const ts=Date.now();

var docClient = new AWS.DynamoDB.DocumentClient();
//dummy
app.get('/', (req, res) => {
    console.log(req.query);
  res.send('Get started');
});

initDatabase();

//api to create and submit username
app.get('/userfinal', async(req,res) => {
    console.log(req.query)
    try {
      await docClient.put({
        TableName: "to-do",
        Item: {
          user_id: ts.toString(),
          username: req.query.username,
        },
      });
      res.send({
        user_id: ts,
        message: "creating user<" + req.query.username + "> is success",
        task:"your task : "+req.query.task
      });
    } catch (error) {
      console.log(error);
      res.send({
        error: "creating username failed",
      });
    }
  });


 /* //api for fetching complete and incomplete todo
var params = {
    TableName: "to-do",
    Key:{
        "username": req.query,
        "status": req.query,
    }
  };
  app.get('/fetchtodo', async (req : Request,res : Response) => {
  
    try{
      await docClient.get(params);
      var passvariable = req.query.username.toString();
      res.send(fetchTodo(passvariable));
    }
    catch (error) {
      console.log(error);
      res.send({
        error: "creating username failed",
      });
    }
  });

  //api for adding todo


var params2 = 
{
  TableName : "to-do",
  Item:{
    "username" : req.query,
    "task": req.query,
  }
};
app.get('/addtodo', async(req, res )=> {
  console.log("Username :" ,req.query.username);
  console.log("task :",req.query.task);
 try{
    await docClient.put({TableName : "to-do",
    Item:{
      username : req.query.username,
      task: req.query.task,
    } });
  res.send({
    user_id: ts,
    username: req.query.username,
    task : req.query.task,
    message: "Added task is : "+ req.query.username ,
  });
}
   catch (error) {
  console.log(error);
  res.send({
    error: "Adding Todo Failed",
  });
}
});*/


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })