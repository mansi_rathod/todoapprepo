/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Route, Switch } from "react-router-dom";
import Middlepage from './Middlepage'
import MyForm from './firstpage'
import thirdpage from './thirdpage'

function RouterApp() {
    return (
      <div className="container-fluid">
        <Switch>
          <Route exact path="/" component={MyForm} />
          <Route path="/view-todo" component={Middlepage} />
          <Route path="/add-todo" component={thirdpage} />
        </Switch>
      </div>
    );
  }
  
  export default RouterApp;
  