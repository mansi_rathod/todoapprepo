/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-empty-interface */
import React from 'react';
import ReactDOM from 'react-dom'; 
import { Link } from 'react-router-dom';
interface iprops{}
interface istate{
  username : string;
}
class MyForm extends React.Component<iprops,istate> {
  constructor(props:iprops) {
    super(props);
    this.state = { username: '' };
  }
  mySubmitHandler = (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    alert("You are submitting " + this.state.username);
    }
  myChangeHandler = (event: { target: { value: any; }; }) => {
    this.setState({username: event.target.value});
  }
  render() {
    return (
      <form onSubmit={this.mySubmitHandler}>

     <div className="abc">
       <h1>Welcome to TODO App</h1> 
      <p>Provide your Username:</p>
     
<input
        type='text'
        onChange={this.myChangeHandler}></input>
 <Link to ="/view-todo">
<input type="button" value ="submit"/> </Link></div>
      </form>
    );
  }
}
export default MyForm

